<?php
function labNumber ($number)
{
    $labNumber = false;
    //вычисления берем по модулю
    $number = abs($number);

    for ($i=1;$i<=$number;$i++)
    {
        if ($number % $i == 0 and primeDivisor($i)==true and $number % ($i*$i) == 0)
        {
            $labNumber = true;
            break;
        }
    }

    return $labNumber;
}

function rangeLabNumbers($numberStart, $numberStop)
{
    for ($i=$numberStart; $i<=$numberStop; $i++)
    {
        if (labNumber($i)==true)
        {
            echo $i." ";
        }
    }
}

// определяет простое число или нет
function primeDivisor ($number)
{
    // ищем по модулю
    $number = abs($number);
    $countNumber = 0;
    for ($i = 1; $i<=$number; $i++)
    {
        if ($number%$i==0)
        {
            $countNumber++;
        }
    }

    if ($countNumber == 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lab Numbers</title>

    <link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Lab Numbers</h1>
<form method="post" id="one-number">
    <h2>Принадлежность числа к Lab Numbers</h2>
    <label for="number">Input number (введите число)</label>
    <input id="number" type="number" name="number" value="<?php echo $_POST['number']?>" required>
    <input type="submit" name="submit">
</form>

<?php
if (isset($_POST['submit']))
{
    if (labNumber($_POST['number']) == true)
    {
        echo "true";
    }
    else
    {
        echo "false";
    }
}
?>

<form method="post">
    <h2>Вывод всех Lab Numbers из диапазона</h2>
    <label>Укажите диапазон чисел</label>
    <label for="range1">От</label>
    <input type="number" name="range1" id="range1" required>
    <label for="range2">До</label>
    <input type="number" name="range2" id="range2" required>
    <input type="submit" name="submitRange">
</form>

<?php
if (isset($_POST['submitRange']))
{
    rangeLabNumbers($_POST['range1'],$_POST['range2']);
}
?>

</body>
</html>